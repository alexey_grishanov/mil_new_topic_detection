import ast
from collections import Counter
import glob
import os
import re

import artm
import numpy as np

def read_collection(target_folder, vw_name):
    if len(glob.glob(os.path.join(target_folder, '*.batch'))) < 1:
        batch_vectorizer = artm.BatchVectorizer(
            data_path=vw_name,
            data_format='vowpal_wabbit',
            target_folder=target_folder)
    else:
        batch_vectorizer = artm.BatchVectorizer(
            data_path=target_folder,
            data_format='batches')

    dictionary = artm.Dictionary()
    dict_path = os.path.join(target_folder, 'dict.dict')

    if not os.path.isfile(dict_path):
        dictionary.gather(data_path=batch_vectorizer.data_path)
        dictionary.save(dictionary_path=dict_path)

    dictionary.load(dictionary_path=dict_path)
    return batch_vectorizer, dictionary

def df2vw(dataset, vw_path):
    """
    Converts dataframe with columns 'lemmatized' and 'topmine' to vw file.

    Parameters
    ----------
    dataset : pd.DataFrame
        should contains columns 'lemmatized' and 'topmine'
    vw_path : str
        path to the vowpal wabbit file

    """
    bow_messages = dict()

    for elem in dataset.itertuples():
        bow_messages[elem.Index] = {'lemmatized': Counter(), 'topmine': Counter()}

        if isinstance(elem.lemmatized, str):
            tokens = elem.lemmatized.split()
            tokens = [re.sub('[\:\|\@]', '', token) for token in tokens if re.match]
            bow_messages[elem.Index]['lemmatized'] += Counter(tokens)

        if isinstance(elem.topmine, str):
            tokens_ngramms = elem.topmine
            tokens_ngramms = [re.sub('[\:\|\@]', '', token_ngramm) for token_ngramm in ast.literal_eval(tokens_ngramms)]
            tokens_ngramms = list(filter(None, tokens_ngramms))
            bow_messages[elem.Index]['topmine'] += Counter(tokens_ngramms)
            
    bow_messages_str_format = []

    for ind in bow_messages.keys():
        message_str_format = str(ind)
        for modality in bow_messages[ind].keys():
            modality_content = " ".join([
                token + ':' + str(count)
                for token, count in bow_messages[ind][modality].items()
            ])
            message_str_format += " |{} {}".format(modality, modality_content)
        message_str_format += " |time_n {}".format(ind)
        bow_messages_str_format.append(message_str_format)
        
    with open(vw_path, 'w') as f:
        for elem in bow_messages_str_format:
            f.write(elem + '\n')
                
def compute_pt_distribution(model):
    n_wt = model.get_phi(class_ids='lemmatized')
    n_t = n_wt.sum(axis=0)
    return n_t / n_t.sum()

def compute_joint_pwt_distribution(phi, p_t):
    joint_pwt = p_t[:, np.newaxis] * phi.transpose()
    return joint_pwt

def compute_ptw(joint_pwt):
    return joint_pwt / np.sum(joint_pwt, axis=0)  # sum by all T


def read_plaintext(line):
    modals = line.split("|")
    doc_num = int(modals[0].strip())
    data = modals[1].strip().split(" ")[1:]
    return doc_num, data

def tw_vectorized(words, phi_val, phi_rows):
    wt = np.zeros((len(words), 5)) # theme counter for all the words
    for i, word in enumerate(words):
        word = word.split(':')[0]
        if word not in phi_rows:
            continue
        local_wt = phi_val.iloc[phi_rows.index(word), :].values
        wt[i, :] = local_wt
    return wt

def compute_features_from_pwt2(docs_ptw, doc_id):
    """First feature."""
    bins = np.arange(0.1, 1.01, 0.1)
    prob_matrix = docs_ptw[doc_id][0]
    features = np.zeros(50)
    for word_ind in range(len(prob_matrix)):
        for theme in range(5):
            word_bin = np.digitize(prob_matrix[word_ind, theme], bins)
            if word_bin == 10:
                word_bin = 9
            features[word_bin + theme * 10] += 1
    features /= len(prob_matrix)
    # np.sum(features) must be = 5
    return features

def compute_features_from_pwt4(docs_ptw, doc_id):
    """Second feature."""
    prob_matrix = docs_ptw[doc_id][0]
    features = np.mean(prob_matrix, axis=0)
    # np.sum(features) must be = 5
    return features
