# Tracking of new topics

---

Code for MIL summer 2019 internship 

---

### Usage:

Look through [demonstration notebook](./`new_topic_detection.ipynb). It contains computing 2 types of features and running classifier over it.

